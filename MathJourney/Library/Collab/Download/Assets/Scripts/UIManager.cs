﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;
using Slider = UnityEngine.UI.Slider;
using Random = UnityEngine.Random;

public class UIManager : Singleton<UIManager>
{

    #region ------------------ Editor Elements ------------------

    [Header("Main UI Pages")]
    public GameObject welcomePage;
    public GameObject loginPage;
    public GameObject infoPage;
    public GameObject rocketColorPage;
    public GameObject numberDigitSelectionPage;
    public GameObject operationSelectionPage;
    public GameObject inGamePage;
    public GameObject pausePage;
    public GameObject endGamePage;
    public GameObject animPage;

    [Header("Login Page")]
    public GameObject playerName_Input;
    public GameObject playerAge_Input;
    public GameObject nextPageButton_Login;
    public GameObject previousPageButton_Login;

    [Header("Rocket Color Selection Page")]
    public GameObject rocket_original;
    public GameObject rocket_original_d;
    public GameObject rocket_yellow;
    public GameObject rocket_yellow_d;
    public GameObject rocket_blue;
    public GameObject rocket_blue_d;
    public GameObject rocket_green;
    public GameObject rocket_green_d;
    public GameObject nextPageButton_RocektColor;
    public GameObject previousPageButton_RocketColor;
    public GameObject rocketPlatformCamera;
    public GameObject rocketsSpritesParent;
    public GameObject shadowsSpritesParent;

    [Header("Number Selection Page")]
    public GameObject oneDigitButton;
    public GameObject twoDigitButton;
    public GameObject nextPageButton_DigitSelect;
    public GameObject previousPageButton_DigitSelect;

    [Header("Operation Selection Page")]
    public GameObject nextPageButton_OpSelect;
    public GameObject previousPageButton_OpSelect;
    public GameObject planetPlatformCamera;
    public GameObject planetsSpritesParent;

    [Header("In-Game Page")]
    public GameObject speedSlider;
    public GameObject fuelSlider;
    public GameObject mainScoreText_InGame;
    public GameObject countdownText_InGame;
    public GameObject additionalScoreText_InGame;
    public GameObject pauseButton_InGame;
    public GameObject soundButton_InGame;
    public GameObject slowMotionEffect_anim;

    [Header("End-Game Page")]
    public UnityEngine.UI.Image winBG;
    public UnityEngine.UI.Image lostBG;
    public GameObject totalScoreText_EndGame;
    public GameObject correctAnswersText_EndGame;
    public GameObject mistakesText_EndGame;

    [Header("Start-Finish Animation Page")]
    public GameObject fuelBar_Animator;
    public GameObject countdown_Animator;
    public GameObject levelEndAnimator_1;
    public GameObject levelEndAnimator_2;
    public GameObject levelEndAnimator_3;
    public GameObject levelEndAnimator_4;
    public GameObject levelStartAnimator_1;
    public GameObject levelStartAnimator_2;
    public GameObject levelStartAnimator_3;
    public GameObject levelStartAnimator_4;

    #endregion

    #region ------------------ Hidden & Private Objects ------------------

    [HideInInspector] public GameObject gameEnvironment;
    [HideInInspector] public GameObject level;
    [HideInInspector] public GameObject rocket;
    [HideInInspector] public GameObject rocketBody;
    [HideInInspector] public GameObject rocketBodyDamaged;
    [HideInInspector] public PlayerData currentPlayer;
    [HideInInspector] public Material chosenMaterial;
    [HideInInspector] public bool isOperationSelected;
    [HideInInspector] public bool isSoundOff;
    [HideInInspector] public bool isCollided = false;
    [HideInInspector] public bool isCrashed = false;
    [HideInInspector] public bool isRocketDamaged = false;
    [HideInInspector] public Transform[] rocketsTransforms;
    [HideInInspector] public Transform[] shadowsTransforms;
    private TextMeshProUGUI m_tmpObj;
    private bool isSwiping = false;
    #endregion



    #region ------------------ MonoBehavior Functions ------------------

    void Start()
    {
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        gameEnvironment = GameObject.FindGameObjectWithTag("Game_Environment");
        level = GameObject.FindGameObjectWithTag("Level");
        rocket = GameObject.FindGameObjectWithTag("Rocket");
        rocketBody = GameObject.FindGameObjectWithTag("rocketBody");
        rocketBodyDamaged = GameObject.FindGameObjectWithTag("rocketBodyDamaged");
        isOperationSelected = false;
        isSoundOff = false;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        gameEnvironment = GameObject.FindGameObjectWithTag("Game_Environment");
        level = GameObject.FindGameObjectWithTag("Level");
        rocket = GameObject.FindGameObjectWithTag("Rocket");
        rocketBody = GameObject.FindGameObjectWithTag("rocketBody");
        rocketBodyDamaged = GameObject.FindGameObjectWithTag("rocketBodyDamaged");
        SetRocketColorSet();
        isRocketDamaged = false;

        gameObject.GetComponent<LevelPoolControl>().levelPrefab.GetComponent<QuestionPoolControl>().ResetQuestionNumber();
        gameObject.GetComponent<LevelPoolControl>().levelPrefab.GetComponent<QuestionPoolControl>().SetTheFirstQuestion();
        GameplayManager.Instance.StartTheGame();
    }

    #endregion



    #region ------------------ Welcome Page Functions ------------------

    public void SwitchPages_Welcome_Next()
    {
        currentPlayer = new PlayerData();

        SwitchPages(welcomePage, loginPage);

    }

    #endregion

    #region  ------------------ Login Page Functions ------------------

    public void SwitchPages_Login_Next()
    {
        SubmitPlayerInfo();
        SwitchPages(loginPage, infoPage);
    }

    public void SwitchPages_Login_Previous()
    {
        SwitchPages(loginPage, welcomePage);
    }

    public void SubmitPlayerInfo()
    {
        currentPlayer.playerName = playerName_Input.GetComponent<TMP_InputField>().text;
        currentPlayer.playerAge = int.Parse(playerAge_Input.GetComponent<TMP_InputField>().text);
        currentPlayer.playerScore = 0;
        currentPlayer.answersCount = 0;
        currentPlayer.mistakesCount = 0;
    }

    #endregion

    #region ------------------ Info Page Functions ------------------

    public void SwitchPages_Info_Next()
    {
        SwitchPages(infoPage, rocketColorPage);
    }

    public void SwitchPages_Info_Previous()
    {
        SwitchPages(infoPage, loginPage);
    }

    #endregion

    #region ------------------ Rocket Color Page Functions ------------------

    public void SetRocket_Button()
    {
        Debug.Log(Mathf.RoundToInt(rocketsSpritesParent.transform.eulerAngles.y));

        switch (Mathf.RoundToInt(rocketsSpritesParent.transform.eulerAngles.y))
        {
            case 0:
                currentPlayer.rocketColorSet = 1;
                SetRocketColorSet();
                SwitchPages_RocketColor_Next();
                break;
            case 270:
                currentPlayer.rocketColorSet = 2;
                SetRocketColorSet();
                SwitchPages_RocketColor_Next();
                break;
            case 90:
                currentPlayer.rocketColorSet = 3;
                SetRocketColorSet();
                SwitchPages_RocketColor_Next();
                break;
            case 180:
                currentPlayer.rocketColorSet = 4;
                SetRocketColorSet();
                SwitchPages_RocketColor_Next();
                break;
        }
    }

    public void SetRocketColorSet()
    {
        Destroy(rocketBody);
        Destroy(rocketBodyDamaged);

        switch (currentPlayer.rocketColorSet)
        {
            case 1:
                rocketBody = Instantiate(rocket_original, rocket.transform);
                rocketBodyDamaged = Instantiate(rocket_original_d, rocket.transform);
                rocketBodyDamaged.SetActive(false);
                break;

            case 2:
                rocketBody = Instantiate(rocket_yellow, rocket.transform);
                rocketBodyDamaged = Instantiate(rocket_yellow_d, rocket.transform);
                rocketBodyDamaged.SetActive(false);
                break;
            
            case 3:
                rocketBody = Instantiate(rocket_green, rocket.transform);
                rocketBodyDamaged = Instantiate(rocket_green_d, rocket.transform);
                rocketBodyDamaged.SetActive(false);
                break;

            case 4:
                rocketBody = Instantiate(rocket_blue, rocket.transform);
                rocketBodyDamaged = Instantiate(rocket_blue_d, rocket.transform);
                rocketBodyDamaged.SetActive(false);
                break;
        }
    }

    public void RotateRocketsBySwiping()
    {
        StartCoroutine(SwipeRocket_co());
    }

    IEnumerator SwipeRocket_co()
    {
        string direction = GameplayManager.Instance.Swipe();
        
        if(direction == "left" && !isSwiping)
        {
            isSwiping = true;
            rocketsSpritesParent.transform.DORotate(rocketsSpritesParent.transform.eulerAngles + new Vector3(0, 90, 0), 0.5f);
            shadowsSpritesParent.transform.DORotate(shadowsSpritesParent.transform.eulerAngles + new Vector3(0, 90, 0), 0.5f);
            rocketsTransforms = rocketsSpritesParent.GetComponentsInChildren<Transform>();
            shadowsTransforms = shadowsSpritesParent.GetComponentsInChildren<Transform>();
            for (int i = 1; i < rocketsTransforms.Length; i++)
            {
                rocketsTransforms[i].DOLocalRotate(rocketsTransforms[i].transform.localEulerAngles + new Vector3(0, 270, 0), 0.5f);
                shadowsTransforms[i].DOLocalRotate(shadowsTransforms[i].transform.localEulerAngles + new Vector3(0, 270, 0), 0.5f);
            }
            
            yield return new WaitForSeconds(0.5f);
            isSwiping = false;
        }
        else
        if(direction == "right" && !isSwiping)
        {
            isSwiping = true;
            rocketsSpritesParent.transform.DORotate(rocketsSpritesParent.transform.eulerAngles + new Vector3(0, 270, 0), 0.5f);
            shadowsSpritesParent.transform.DORotate(shadowsSpritesParent.transform.eulerAngles + new Vector3(0, 270, 0), 0.5f);
            rocketsTransforms = rocketsSpritesParent.GetComponentsInChildren<Transform>();
            shadowsTransforms = shadowsSpritesParent.GetComponentsInChildren<Transform>();
            for (int i = 1; i < rocketsTransforms.Length; i++)
            {
                rocketsTransforms[i].DOLocalRotate(rocketsTransforms[i].transform.localEulerAngles + new Vector3(0, 90, 0), 0.5f);
                shadowsTransforms[i].DOLocalRotate(shadowsTransforms[i].transform.localEulerAngles + new Vector3(0, 90, 0), 0.5f);
            }
            
            yield return new WaitForSeconds(0.5f);
            isSwiping = false;
        }

    }

    public void SwitchPages_RocketColor_Next()
    {
        StartCoroutine(SwitchPages_RocketColor_Next_co());
    }

    IEnumerator SwitchPages_RocketColor_Next_co()
    {
        SetRocketColorSet();
        yield return new WaitForSeconds(SFXManager.Instance.rocketButtonClickAS.clip.length);
        SwitchPages(rocketColorPage, operationSelectionPage);
    }

    public void SwitchPages_RocketColor_Previous()
    {
        SwitchPages(rocketColorPage, infoPage);
    }

    #endregion

    #region ------------------ Operation Selection Page Functions ------------------

    public void SetOperation_Button()
    {
        switch (Mathf.FloorToInt(planetsSpritesParent.transform.eulerAngles.y))
        {
            case 0:
                GameplayManager.Instance.chosenOperation = GameplayManager.Operation.addition;
                SwitchPages_OpSelect_Next();
                break;
            case 240:
                GameplayManager.Instance.chosenOperation = GameplayManager.Operation.subtraction;
                SwitchPages_OpSelect_Next();
                break;
            case 120:
                GameplayManager.Instance.chosenOperation = GameplayManager.Operation.multiplication;
                SwitchPages_OpSelect_Next();
                break;
        }
            
        nextPageButton_OpSelect.SetActive(true);
    }

    public void RotatePlanetsBySwiping()
    {
        StartCoroutine(SwipePlanet_co());
    }

    IEnumerator SwipePlanet_co()
    {
        string direction = GameplayManager.Instance.Swipe();

        if (direction == "left" && !isSwiping)
        {
            isSwiping = true;
            planetsSpritesParent.transform.DORotate(planetsSpritesParent.transform.eulerAngles + new Vector3(0, 120, 0), 0.5f);

            yield return new WaitForSeconds(0.5f);
            isSwiping = false;
        }
        else if (direction == "right" && !isSwiping)
             {
                isSwiping = true;
                planetsSpritesParent.transform.DORotate(planetsSpritesParent.transform.eulerAngles + new Vector3(0, -120, 0), 0.5f);

                yield return new WaitForSeconds(0.5f);
                isSwiping = false;
             }
    }

    public void SwitchPages_OpSelect_Next()
    {
        StartCoroutine(SwitchPages_OpSelect_Next_co());
        
    }

    IEnumerator SwitchPages_OpSelect_Next_co()
    {
        yield return new WaitForSeconds(SFXManager.Instance.planetButtonClickAS.clip.length);
        SwitchPages(operationSelectionPage, numberDigitSelectionPage);
    }

    public void SwitchPages_OpSelect_Previous()
    {
        SwitchPages(operationSelectionPage, rocketColorPage);
    }

    #endregion

    #region ------------------ Number Digit Selection Page Functions ------------------

    public void SetNumberDigit()
    {
        GameObject selectedButton = EventSystem.current.currentSelectedGameObject;
        string buttonText = selectedButton.name;

        switch (buttonText)
        {
            case "1":
                GameplayManager.Instance.chosenLevel = 1;
                SwitchPages_NumberSelect_Next();
                break;
            case "2":
                GameplayManager.Instance.chosenLevel = 2;
                SwitchPages_NumberSelect_Next();
                break;
            case "3":
                GameplayManager.Instance.chosenLevel = 3;
                SwitchPages_NumberSelect_Next();
                break;
            case "4":
                GameplayManager.Instance.chosenLevel = 4;
                SwitchPages_NumberSelect_Next();
                break;
            case "5":
                GameplayManager.Instance.chosenLevel = 5;
                SwitchPages_NumberSelect_Next();
                break;
            case "6":
                GameplayManager.Instance.chosenLevel = 6;
                SwitchPages_NumberSelect_Next();
                break;
            case "7":
                GameplayManager.Instance.chosenLevel = 7;
                SwitchPages_NumberSelect_Next();
                break;
            case "8":
                GameplayManager.Instance.chosenLevel = 8;
                SwitchPages_NumberSelect_Next();
                break;
            case "9":
                GameplayManager.Instance.chosenLevel = 9;
                SwitchPages_NumberSelect_Next();
                break;
            case "All":
                GameplayManager.Instance.chosenLevel = 12;
                SwitchPages_NumberSelect_Next();
                break;
        }
        nextPageButton_DigitSelect.SetActive(true);
    }

    public void SwitchPages_NumberSelect_Next()
    {
        StartCoroutine(SwitchPages_NumberSelect_Next_co());
    }

    IEnumerator SwitchPages_NumberSelect_Next_co()
    {
        yield return new WaitForSeconds(SFXManager.Instance.numberButtonClickAS.clip.length);
        
        if (GameplayManager.Instance.chosenLevel != 1)
        {
            SceneManager.LoadScene(GameplayManager.Instance.chosenLevel - 1);
        }
        gameObject.GetComponent<LevelPoolControl>().levelPrefab.GetComponent<QuestionPoolControl>().ResetQuestionNumber();
        gameObject.GetComponent<LevelPoolControl>().levelPrefab.GetComponent<QuestionPoolControl>().SetTheFirstQuestion();
        SwitchPages(numberDigitSelectionPage, inGamePage);
        GameplayManager.Instance.StartTheGame();
    }

    public void SwitchPages_NumberSelect_Previous()
    {
        SwitchPages(numberDigitSelectionPage, operationSelectionPage);
    }

    #endregion

    #region ------------------ In-Game Page Functions ------------------

    public void SwitchPages_InGame_EndGame()
    {
        totalScoreText_EndGame.GetComponent<TMPro.TextMeshProUGUI>().text = mainScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().text;
        SetEndGameTexts();
        
        if(currentPlayer.mistakesCount < 4)
        {
            winBG.gameObject.SetActive(false);
            winBG.gameObject.SetActive(true);
        }
        else
        {
            lostBG.gameObject.SetActive(true);
            winBG.gameObject.SetActive(false);

        }

        SwitchPages(inGamePage, endGamePage);
    }

    public void SwitchPages_InGame_Pause()
    {
        GameplayManager.Instance.StopTheGame();
        SwitchPages(inGamePage, pausePage);
    }

    public void SetScore(float number)
    {
        currentPlayer.playerScore = (number < 0) ? 0 : Mathf.RoundToInt(number);
        mainScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText(number.ToString());
    }

    public void SetTimer(float number)
    {

        countdownText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText(Mathf.RoundToInt(number).ToString());
    }

    public void AddScore(bool isAnswerCorrect)
    {
        StartCoroutine(AddScore_Coroutine(isAnswerCorrect));
    }

    IEnumerator AddScore_Coroutine(bool isAnswerCorrect)
    {
        if (isAnswerCorrect)
        {
            GameplayManager.Instance.additionalScore += 500;
            currentPlayer.answersCount++;
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText("+ 500");
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().color = Color.green;
        }
        else if (!isAnswerCorrect)
        {
            GameplayManager.Instance.additionalScore -= 250;
            currentPlayer.mistakesCount++;
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText("- 250");
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().color = Color.red;
        }

        yield return new WaitForSeconds(1f);
        additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText("");
    }
    
    public void AddBonusScore(string type)
    {
        StartCoroutine(AddBonusScore_Coroutine(type));
    }

    IEnumerator AddBonusScore_Coroutine(string st)
    {
        if (st == "barrel")
        {
            GameplayManager.Instance.additionalScore += 500;
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText("+ 500");
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().color = Color.green;
        }
        else if ( st == "obstacle")
        {
            GameplayManager.Instance.additionalScore -= 250;
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText("- 250");
            additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().color = Color.red;
        }

        yield return new WaitForSeconds(1f);
        additionalScoreText_InGame.GetComponent<TMPro.TextMeshProUGUI>().SetText("");
    }

    #endregion

    #region ------------------ Pause Page Functions ------------------

    public void SwitchPages_Pause_BackToGame()
    {
        SwitchPages(pausePage, inGamePage);
        SFXManager.Instance.gameplayTheme1AS.Play();
        GameplayManager.Instance.StartTheGameAfter(3f);
    }

    public void SwitchPages_Pause_Home()
    {
        GameplayManager.Instance.ResetTheGame();
        SwitchPages(pausePage, welcomePage);
    }

    public void SwitchPages_Pause_RestartTheGame()
    {
        GameplayManager.Instance.ResetTheGame();
        SwitchPages(pausePage, inGamePage);
        GameplayManager.Instance.StartTheGame();
    }

    #endregion

    #region ------------------ End Game Page Functions ------------------

    public void SwitchPages_EndGame_Home()
    {
        GameplayManager.Instance.ResetTheGame();
        SwitchPages(endGamePage, welcomePage);
    }

    public void SwitchPages_EndGame_RestartTheGame()
    {
        GameplayManager.Instance.ResetTheGame();
        SwitchPages(endGamePage, inGamePage);
        GameplayManager.Instance.StartTheGame();
    }

    public void GoNextLevel()
    {
        GameplayManager.Instance.chosenLevel = (GameplayManager.Instance.chosenLevel + 1) % 13 == 0 ? 1 : (GameplayManager.Instance.chosenLevel + 1) % 13;
        //GameplayManager.Instance.GoNextLevel_Endof9();
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % 12);
        SwitchPages(endGamePage, inGamePage);
        GameplayManager.Instance.ResetTheGame();
        GameplayManager.Instance.StartTheGame();
    }

    public void SetEndGameTexts()
    {
        totalScoreText_EndGame.GetComponent<TMPro.TextMeshProUGUI>().SetText(currentPlayer.playerScore.ToString());
        correctAnswersText_EndGame.GetComponent<TMPro.TextMeshProUGUI>().SetText(currentPlayer.answersCount.ToString());
        mistakesText_EndGame.GetComponent<TMPro.TextMeshProUGUI>().SetText(currentPlayer.mistakesCount.ToString());
    }

    #endregion

    #region ------------------ Start-Finish Animations Page Functions ------------------
    
    public void RunStartAnimation()
    {
        StartCoroutine(RunStartAnimation_co());
    }

    IEnumerator RunStartAnimation_co()
    {
        animPage.SetActive(true);
        level.SetActive(false);
        rocket.SetActive(false);
        inGamePage.SetActive(false);
        m_tmpObj = countdown_Animator.GetComponent<TMPro.TextMeshProUGUI>();
        
        switch (currentPlayer.rocketColorSet)
        {
            case 1:
                levelStartAnimator_1.SetActive(true);
                SFXManager.Instance.PlayStartAnimSFX();
                yield return new WaitForSeconds(3f);
                levelStartAnimator_1.SetActive(false);
                animPage.SetActive(true);
                inGamePage.SetActive(true);
                level.SetActive(true);
                rocket.SetActive(true);
                countdown_Animator.SetActive(true);
                m_tmpObj.color = new Color32(255, 47, 0, 255);
                m_tmpObj.text = "3";
                countdown_Animator.transform.DOScale(2f, 0.5f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.5f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(232, 255, 0, 255);
                m_tmpObj.text = "2";
                countdown_Animator.transform.DOScale(2f, 0.5f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.5f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "1";
                countdown_Animator.transform.DOScale(2f, 0.5f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.5f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "BAŞLA!";
                countdown_Animator.transform.DOScale(2f, 0.5f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.5f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.text = "";
                countdown_Animator.SetActive(false);
                animPage.SetActive(false);
                yield return new WaitForSeconds(0.5f);
                break;

            case 2:
                levelStartAnimator_2.SetActive(true);
                SFXManager.Instance.PlayStartAnimSFX();
                yield return new WaitForSeconds(3f);
                levelStartAnimator_2.SetActive(false);
                animPage.SetActive(true);
                inGamePage.SetActive(true);
                level.SetActive(true);
                rocket.SetActive(true);
                countdown_Animator.SetActive(true);
                m_tmpObj.color = new Color32(255, 47, 0, 255);
                m_tmpObj.text = "3";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(232, 255, 0, 255);
                m_tmpObj.text = "2";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                countdown_Animator.transform.localScale = Vector3.zero;
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "1";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "BAŞLA!";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.text = "";
                countdown_Animator.SetActive(false);
                animPage.SetActive(false);
                yield return new WaitForSeconds(0.5f);
                break;

            case 3:
                levelStartAnimator_3.SetActive(true);
                SFXManager.Instance.PlayStartAnimSFX();
                yield return new WaitForSeconds(3f);
                levelStartAnimator_3.SetActive(false);
                animPage.SetActive(true);
                inGamePage.SetActive(true);
                level.SetActive(true);
                rocket.SetActive(true);
                countdown_Animator.SetActive(true);
                m_tmpObj.color = new Color32(255, 47, 0, 255);
                m_tmpObj.text = "3";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(232, 255, 0, 255);
                m_tmpObj.text = "2";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "1";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "BAŞLA!";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.text = "";
                countdown_Animator.SetActive(false);
                animPage.SetActive(false);
                yield return new WaitForSeconds(0.5f);
                break;

            case 4:
                levelStartAnimator_4.SetActive(true);
                SFXManager.Instance.PlayStartAnimSFX();
                yield return new WaitForSeconds(3f);
                levelStartAnimator_4.SetActive(false);
                animPage.SetActive(true);
                inGamePage.SetActive(true);
                level.SetActive(true);
                rocket.SetActive(true);
                countdown_Animator.SetActive(true);
                m_tmpObj.color = new Color32(255, 47, 0, 255);
                m_tmpObj.text = "3";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(232, 255, 0, 255);
                m_tmpObj.text = "2";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "1";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.color = new Color32(70, 209, 88, 255);
                m_tmpObj.text = "BAŞLA!";
                countdown_Animator.transform.DOScale(2f, 0.8f).SetEase(Ease.OutSine);
                countdown_Animator.transform.DOScale(0f, 0.2f).SetEase(Ease.OutSine).SetDelay(0.8f);
                yield return new WaitForSeconds(1f);
                m_tmpObj.text = "";
                countdown_Animator.SetActive(false);
                animPage.SetActive(false);
                yield return new WaitForSeconds(0.5f);
                break;
        }

        yield return null;
    }

    public void RunFinishAnimation()
    {
        StartCoroutine(RunFinishAnimation_co());
    }

    IEnumerator RunFinishAnimation_co()
    {
        animPage.SetActive(true);
        level.SetActive(false);
        rocket.SetActive(false);
        inGamePage.SetActive(false);
        switch (currentPlayer.rocketColorSet)
        {
            case 1:
                levelEndAnimator_1.SetActive(true);
                SFXManager.Instance.PlayLevelEndAnimSFX();
                yield return new WaitForSeconds(3f);
                fuelBar_Animator.SetActive(true);
                yield return new WaitForSeconds(2.5f);
                levelEndAnimator_1.SetActive(false);
                fuelBar_Animator.SetActive(false);
                animPage.SetActive(false);
                endGamePage.SetActive(true);
                break;
            case 2:
                levelEndAnimator_2.SetActive(true);
                SFXManager.Instance.PlayLevelEndAnimSFX();
                yield return new WaitForSeconds(3f);
                fuelBar_Animator.SetActive(true);
                yield return new WaitForSeconds(2.5f);
                levelEndAnimator_2.SetActive(false);
                fuelBar_Animator.SetActive(false);
                animPage.SetActive(false);
                endGamePage.SetActive(true);
                break;
            case 3:
                levelEndAnimator_3.SetActive(true);
                SFXManager.Instance.PlayLevelEndAnimSFX();
                yield return new WaitForSeconds(3f);
                fuelBar_Animator.SetActive(true);
                yield return new WaitForSeconds(2.5f);
                levelEndAnimator_3.SetActive(false);
                fuelBar_Animator.SetActive(false);
                animPage.SetActive(false);
                endGamePage.SetActive(true);
                break;
            case 4:
                levelEndAnimator_4.SetActive(true);
                SFXManager.Instance.PlayLevelEndAnimSFX();
                yield return new WaitForSeconds(3f);
                fuelBar_Animator.SetActive(true);
                yield return new WaitForSeconds(2.5f);
                levelEndAnimator_4.SetActive(false);
                fuelBar_Animator.SetActive(false);
                animPage.SetActive(false);
                endGamePage.SetActive(true);
                break;
        }

        yield return null;
        SwitchPages_InGame_EndGame();
    }

    #endregion

    #region ------------------ Common Functions ------------------

    public void SwitchPages(GameObject currentPage, GameObject nextPage)
    {
        nextPage.SetActive(true);
        currentPage.SetActive(false);
    }

    public void PlayCommonButtonClickSFX()
    {
        SFXManager.Instance.commonButtonClickAS.Play();
    }

    public void PlaySlowMoEffect()
    {
        StartCoroutine(PlaySlowMoEffect_co());
    }

    IEnumerator PlaySlowMoEffect_co()
    {
        slowMotionEffect_anim.SetActive(true);
        slowMotionEffect_anim.GetComponent<Animator>().Play("SlowMo_Anim");
        yield return new WaitForSeconds(0.7f);
        slowMotionEffect_anim.SetActive(false);
        if(rocketBody.activeInHierarchy)
        {
            rocketBody.GetComponent<RocketElements>().BoostTheRocket();
            yield return new WaitForSeconds(1.5f);
        }
        else if (rocketBodyDamaged.activeInHierarchy)
        {
            rocketBodyDamaged.GetComponent<RocketElements>().BoostTheRocket();
            yield return new WaitForSeconds(1.5f);
        }
    }

    public IEnumerator DoBarrelTriggerEffect(GameObject collider, GameObject barrel, ParticleSystem parEffect, int valueToAdd)
    {
        if (isRocketDamaged)
        {
            rocketBody.SetActive(true);
            rocketBodyDamaged.SetActive(false);
            isRocketDamaged = false;
        }

        if (!isCollided)
        {
            isCollided = true;
            GameplayManager.Instance.StopTheGame_Collision();
            
            if(GameplayManager.Instance.rocket.transform.localPosition.x <= 10)
            {
                Instantiate(parEffect, collider.transform);
                fuelSlider.GetComponent<Slider>().value += valueToAdd;
                AddBonusScore("barrel");
                barrel.transform.localScale = Vector3.zero;
                SFXManager.Instance.PlayFuelBarrelSFX();
                GameplayManager.Instance.StartTheGameAfter(0f);
                while (!CheckDistance(collider.transform.position, barrel.transform.position, 10))
                {
                    yield return null;
                }
            }

            else if (GameplayManager.Instance.rocket.transform.localPosition.x > 10)
            {
                Instantiate(parEffect, collider.transform);
                fuelSlider.GetComponent<Slider>().value += valueToAdd;
                AddBonusScore("barrel");
                barrel.transform.localScale = Vector3.zero;
                SFXManager.Instance.PlayFuelBarrelSFX();
                GameplayManager.Instance.StartTheGameAfter(0);
                while (!CheckDistance(collider.transform.position, barrel.transform.position, 10))
                {
                    yield return null;
                }
            }

            barrel.transform.localScale = Vector3.one;
            isCollided = false;
        }

        yield return null;
    }

    public IEnumerator DoObstacleTriggerEffect(GameObject collider, GameObject obstacle, ParticleSystem parEffect1, ParticleSystem parEffect2, int valueToAdd)
    {
        ParticleSystem effBoom;
        ParticleSystem effPuff;

        if (!isRocketDamaged)
        {
            rocketBody.SetActive(false);
            rocketBodyDamaged.SetActive(true);
            isRocketDamaged = true;
        }

        if (!isCollided)
        {
            isCollided = true;
            isCrashed = true;
            GameplayManager.Instance.StopTheGame_Collision();

            if (GameplayManager.Instance.rocket.transform.localPosition.x > 3)
            {
                effBoom = Instantiate(parEffect1, obstacle.transform.position, collider.transform.rotation);
                effPuff = Instantiate(parEffect2, obstacle.transform.position, collider.transform.rotation);
                effPuff.transform.DOScale(1f, 1f);
                collider.transform.DOPunchRotation(4 * Vector3.one, 1f);
                fuelSlider.GetComponent<UnityEngine.UI.Slider>().value += valueToAdd;
                AddBonusScore("obstacle");
                obstacle.transform.localScale = Vector3.zero;
                SFXManager.Instance.crashAS.Play();
                GameplayManager.Instance.StartTheGameAfter(1f);
                isCrashed = false;
                while(!CheckDistance(collider.transform.position, obstacle.transform.position, 10))
                {
                    yield return null;
                }
            }

            else if (GameplayManager.Instance.rocket.transform.localPosition.x <= 3)
            {
                effBoom = Instantiate(parEffect1, obstacle.transform.position, collider.transform.rotation);
                effPuff = Instantiate(parEffect2, obstacle.transform.position, collider.transform.rotation);
                effPuff.transform.DOScale(1f, 1f);
                collider.transform.DOPunchRotation(4 * Vector3.one, 1f);
                fuelSlider.GetComponent<UnityEngine.UI.Slider>().value += valueToAdd;
                AddBonusScore("obstacle");
                obstacle.transform.localScale = Vector3.zero;
                SFXManager.Instance.crashAS.Play();
                GameplayManager.Instance.StartTheGameAfter(1f);
                isCrashed = false;
                while (!CheckDistance(collider.transform.position, obstacle.transform.position, 10))
                {
                    yield return null;
                }
            }

            obstacle.transform.localScale = 5 * Vector3.one;
            isCollided = false;
        }

        yield return null;
    }

    public bool CheckDistance(Vector3 from, Vector3 to, float value)
    {
        if(Vector3.Distance(from, to) > value)
        {
            return true;
        }

        else
            return false;
    }

    #endregion
}
