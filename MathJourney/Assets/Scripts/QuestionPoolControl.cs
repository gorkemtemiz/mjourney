﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionPoolControl : MonoBehaviour
{
    public List<GameObject> questionPool;

    [HideInInspector] public int currentQuestion;

    private void Awake()
    {
        currentQuestion = 0;
    }

    private void Start()
    {
        ResetQuestionNumber();
    }

    public void SetTheFirstQuestion()
    {
        questionPool[0].GetComponent<QuestionTriggerControl>().CreateQuestion();
    }

    public GameObject NextQuestion()
    {
        currentQuestion = (currentQuestion + 1) % questionPool.Count;
        return questionPool[currentQuestion];
    }

    public void ResetQuestionNumber()
    {
        currentQuestion = 0;
    }
}
