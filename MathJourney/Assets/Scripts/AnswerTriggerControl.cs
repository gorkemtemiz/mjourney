﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerTriggerControl : MonoBehaviour
{
    [Tooltip("The GameObject Of The Question Text")]
    public GameObject q_Object;
    [Tooltip("The GameObject Of The Choice Text")]
    public GameObject c_Text_Object;

    [HideInInspector] public bool isGlowing = false;

    private List<GameObject> answerTextObjects = new List<GameObject>();


    private void OnTriggerEnter(Collider other)
    {
        if (c_Text_Object.GetComponent<TMPro.TMP_Text>().text == q_Object.GetComponent<QuestionTriggerControl>().correctAnswer.ToString())
        {
            SFXManager.Instance.correctAnswerAS.Play();
            UIManager.Instance.AddScore(true);
        }
        else
        {
            SFXManager.Instance.wrongAnswerAS.Play();
            UIManager.Instance.AddScore(false);
        }

        answerTextObjects = q_Object.GetComponent<QuestionTriggerControl>().answerTextGameObjectList;

        foreach (GameObject go in answerTextObjects)
        {
            go.GetComponent<TMPro.TMP_Text>().text = "";
        }
    }
}

