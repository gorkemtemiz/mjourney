﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelPoolControl : Singleton<LevelPoolControl>
{
    [HideInInspector] public GameObject levelPrefab;
    [HideInInspector] public int currentLevel;

    private void Start()
    {
        levelPrefab = GameObject.FindGameObjectWithTag("Level");
        currentLevel = 0;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        currentLevel = 0;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        levelPrefab = GameObject.FindGameObjectWithTag("Level");
    }
}
