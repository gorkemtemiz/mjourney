﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using TMPro;
using DG.Tweening.Plugins.Core.PathCore;
using PathCreation.Examples;
using System.Linq;
using UnityEditor;
using UnityEngine.SceneManagement;
using PathCreation;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;
using System;
using Random = UnityEngine.Random;

public class GameplayManager : Singleton<GameplayManager>
{
    #region ------------------- Editor Elements -------------------

    [Header("Gameplay Parameters")]
    public float sessionCountdown;
    public float maxFuel;
    public float carMinSpeed;
    public float carMaxSpeed;
    public float carAcceleration;
    public float carBreakAcceleration;
    public float carCurrentSpeed;
    public GameObject startLine;

    #endregion

    #region ------------------- Hidden & Private Objects -------------------

    [HideInInspector] public enum Operation { addition, subtraction, multiplication }
    [HideInInspector] public Operation chosenOperation;
    [HideInInspector] public GameObject gameEnvironment;
    [HideInInspector] public GameObject rocket;
    [HideInInspector] public GameObject playerObject;
    [HideInInspector] public int numberDigit = 0;
    [HideInInspector] public int chosenLevel;
    [HideInInspector] public int inLevel_Counter;
    [HideInInspector] public bool isFlameBeingShowed = false;
    [HideInInspector] public bool isCarMovingHorizontally = false;
    [HideInInspector] public bool runTheGame;
    [HideInInspector] public bool isCountingDown;
    [HideInInspector] public float playerScore;
    [HideInInspector] public float roadScore;
    [HideInInspector] public float additionalScore;
    [HideInInspector] public float standartCountdown;
    [HideInInspector] public PathCreation.Examples.PathFollower pathFollower;
    
    private readonly Ease ease = Ease.OutBack;
    private float remainingFuel;
    private float elapsed = 0f;
    private Vector2 firstMousePos;
    private Vector2 lastMousePos;
    private Vector2 currentSwipe;
    private int _random;

    #endregion


    #region ------------------- Monobehaviour Functions -------------------

    private void Start()
    {
        isCountingDown = false;
        standartCountdown = sessionCountdown;
        runTheGame = false;
        playerScore = 0;
        roadScore = 0;
        additionalScore = 0;
        pathFollower = playerObject.GetComponent<PathFollower>();
        pathFollower.speed = carMinSpeed;
        startLine = GameObject.FindGameObjectWithTag("startLine");

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void OnEnable()
    {
       
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    void Update()
    {
        if (gameEnvironment != null && gameEnvironment.activeInHierarchy && runTheGame)
        {
            CountingDown();

            if (UIManager.Instance.fuelSlider.GetComponent<Slider>().value == 0 || inLevel_Counter >= 11)
            {
                runTheGame = false;
                UIManager.Instance.RunFinishAnimation();
            }

            if (Input.GetMouseButtonDown(0))
            {
                firstMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }

            if (Input.GetMouseButtonUp(0))
            {
                lastMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                currentSwipe = new Vector2(lastMousePos.x - firstMousePos.x, lastMousePos.y - lastMousePos.y);
            }

            if ((Input.GetKeyDown(KeyCode.RightArrow) || Swipe() == "right") && (rocket.transform.localPosition.x < 13.5f)) 
            {
                StartCoroutine(MoveTheRocket("R"));
            }

            if ((Input.GetKeyDown(KeyCode.LeftArrow) || Swipe() == "left") && (rocket.transform.localPosition.x > -13.5f))
            {
                StartCoroutine(MoveTheRocket("L"));
            }

            if (Input.GetKey(KeyCode.DownArrow) || Input.GetMouseButton(0))
            {
               pathFollower.speed = Mathf.Clamp(pathFollower.speed - carBreakAcceleration, carMinSpeed, carMaxSpeed);
            }

            else
            {
                pathFollower.speed = Mathf.Clamp(pathFollower.speed + carAcceleration, carMinSpeed, carMaxSpeed);
            }

            carCurrentSpeed = pathFollower.speed;       
            UIManager.Instance.speedSlider.GetComponent<Slider>().value = carCurrentSpeed;

            elapsed += Time.deltaTime;
            if(elapsed >= 1f)
            {
                elapsed %= 1f;
                remainingFuel = UIManager.Instance.fuelSlider.GetComponent<Slider>().value;
                if(remainingFuel > 0)   remainingFuel -= carCurrentSpeed / 50;
                UIManager.Instance.fuelSlider.GetComponent<Slider>().value = remainingFuel;
            }

            roadScore += Mathf.CeilToInt(carCurrentSpeed);
            playerScore = additionalScore + roadScore/1000;
            UIManager.Instance.SetScore(Mathf.RoundToInt(playerScore));
        }
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        gameEnvironment = GameObject.FindGameObjectWithTag("Game_Environment");
        rocket = GameObject.FindGameObjectWithTag("Rocket");
        playerObject = GameObject.FindGameObjectWithTag("Player");
        startLine = GameObject.FindGameObjectWithTag("startLine");
        pathFollower = playerObject.GetComponent<PathFollower>();
        pathFollower.speed = carMinSpeed;
        playerObject.GetComponent<PathFollower>().Reset();
    }

    #endregion


    #region ------------------- Game Control Functions -------------------

    public void StartTheGame()
    {
        Time.timeScale = 1;
        StartCoroutine(StartTheGameCoroutine(7f));
    }

    public void StartTheGameAfter(float time)   // to start the game after a collision
    {
        Time.timeScale = 1;
        StartCoroutine(StartTheGame_Collision_Coroutine(time)); 
    }  

    public IEnumerator StartTheGameCoroutine(float much)
    {
        SFXManager.Instance.mainThemeAS.Stop();
        UIManager.Instance.inGamePage.SetActive(false);
        UIManager.Instance.currentPlayer.mistakesCount = 0;
        UIManager.Instance.currentPlayer.answersCount = 0;
        UIManager.Instance.RunStartAnimation();
        yield return new WaitForSeconds(much);
        StartCoroutine(AnimateStartline());
        yield return new WaitForSeconds(2f);
        SFXManager.Instance.gameplayTheme1AS.Play();
        UIManager.Instance.inGamePage.SetActive(true);
        isCountingDown = true;
        runTheGame = true;
    }

    public IEnumerator StartTheGame_Collision_Coroutine(float much)
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(much);
        isCountingDown = true;
        runTheGame = true;
    }

    public void StopTheGame()
    {
        Time.timeScale = 0;
        isCountingDown = false;
        runTheGame = false;
        SFXManager.Instance.StopAllSounds();
        SFXManager.Instance.PlayMainThemeSFX();
    }

    public void StopTheGame_Collision()
    {
        isCountingDown = false;
        runTheGame = false;
    }

    public void ResetTheGame()
    {
        Time.timeScale = 1;
        isCountingDown = false;
        runTheGame = false;
        sessionCountdown = standartCountdown;
        rocket.transform.localPosition = new Vector3(0, 3, 0);
        ResetStartLine();
        UIManager.Instance.speedSlider.GetComponent<Slider>().value = 0;
        UIManager.Instance.fuelSlider.GetComponent<Slider>().value = maxFuel;
        UIManager.Instance.countdownText_InGame.GetComponent<TMPro.TMP_Text>().text = sessionCountdown.ToString();
        UIManager.Instance.currentPlayer.ResetPlayerStats();
        inLevel_Counter = 0;
        UIManager.Instance.SetScore(Mathf.RoundToInt(playerScore));
        playerObject.GetComponent<PathFollower>().Reset();
        gameObject.GetComponent<LevelPoolControl>().levelPrefab.GetComponent<QuestionPoolControl>().ResetQuestionNumber();
        gameObject.GetComponent<LevelPoolControl>().levelPrefab.GetComponent<QuestionPoolControl>().SetTheFirstQuestion();
    }

    public void CountingDown()
    {

        if (isCountingDown)
        {
            sessionCountdown -= Time.deltaTime;
            UIManager.Instance.SetTimer(sessionCountdown);


            if(sessionCountdown <= 0)
            {
                StopTheGame();
                UIManager.Instance.RunFinishAnimation();
            }
        }
    }

    public void ResetStartLine()
    {
        startLine.transform.position = new Vector3(0, 7, 6);
        startLine.transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    public IEnumerator AnimateStartline()
    {
        startLine.transform.DOLocalMoveY(0, 2f).SetEase(Ease.OutCubic);
        startLine.transform.DOLocalRotate(new Vector3(90, 0, 0), 2).SetEase(Ease.OutCubic);
        SFXManager.Instance.startLineAS.Play();
        yield return new WaitForSeconds(2f);
        UIManager.Instance.pauseButton_InGame.SetActive(true);
    }

    //public Operation CheckOperationEnums(Operation op)
    //{
    //    int _currOpIndx = System.Array.IndexOf(Enum.GetValues(op.GetType()), op);
    //    _random = Random.Range(0, 3);
    //    if (!UIManager.Instance.currentPlayer.playedOperations.Contains(_currOpIndx) && !UIManager.Instance.currentPlayer.playedOperations.Contains(_random))
    //    {
    //        UIManager.Instance.currentPlayer.playedOperations.Add(_currOpIndx);
    //        return (Operation)_random;
    //    }
    //    else
    //    {
    //        int[] availables = Enumerable.Range(0, 3).Except(UIManager.Instance.currentPlayer.playedOperations).ToArray();
    //        UIManager.Instance.currentPlayer.playedOperations.Add(_currOpIndx);
    //        _random = availables[Random.Range(0, availables.Length)];
    //        return (Operation)_random;
    //    }
    //}

    #endregion

    #region ------------------- Rocket Control Functions -------------------

    IEnumerator MoveTheRocket(string side) 
    {
        if (side == "L" && !isCarMovingHorizontally && !UIManager.Instance.isCrashed)
        {
            isCarMovingHorizontally = true;

            if (rocket.transform.localPosition.x == 0f)
            {
                rocket.transform.DOLocalMoveX(-13.5f, 0.3f).SetEase(ease);
            }

            else if (rocket.transform.localPosition.x == 13.5f)
            {
                rocket.transform.DOLocalMoveX(0f, 0.3f).SetEase(ease);
            }

            Rocket_LocalRotate("L");
            yield return new WaitForSeconds(0.1f);
            isCarMovingHorizontally = false;
        }

        if (side == "R" && !isCarMovingHorizontally && !UIManager.Instance.isCrashed)
        {
            isCarMovingHorizontally = true;

            if (rocket.transform.localPosition.x == -13.5f)
            { 
                rocket.transform.DOLocalMoveX(0f, 0.3f).SetEase(ease);
            }

            else if (rocket.transform.localPosition.x == 0f)
            {
                rocket.transform.DOLocalMoveX(13.5f, 0.3f).SetEase(ease);
            }

            Rocket_LocalRotate("R");
            yield return new WaitForSeconds(0.1f);
            isCarMovingHorizontally = false;
        }
    }

    void Rocket_LocalRotate(string side)
    {
        StartCoroutine(Rocket_LocalRotate_Coroutine(side));
    }

    IEnumerator Rocket_LocalRotate_Coroutine(string side)
    {
        if(side == "L")
        {
            rocket.transform.DOLocalRotate(new Vector3(rocket.transform.rotation.x, rocket.transform.rotation.y, 25f), 0.2f).SetEase(Ease.InOutBack);
            yield return new WaitForSeconds(0.2f);
            rocket.transform.DOLocalRotate(Vector3.zero, 0.15f).SetEase(Ease.InOutBack);
        }
        else if(side == "R")
        {
            rocket.transform.DOLocalRotate(new Vector3(rocket.transform.rotation.x, rocket.transform.rotation.y, -25f), 0.2f).SetEase(Ease.InOutBack);
            yield return new WaitForSeconds(0.2f);
            rocket.transform.DOLocalRotate(Vector3.zero, 0.15f).SetEase(Ease.InOutBack);
        }
    }

    public string Swipe()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            return null;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            //save ended touch 2d point
            lastMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(lastMousePos.x - firstMousePos.x, lastMousePos.y - firstMousePos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();

            //swipe upwards
            if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                return "up";
            }
            //swipe down
            else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                return "down";
            }
            //swipe left
            else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                return "left";
            }
            //swipe right
            else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                return "right";
            }
            else
                return null; ;
        }

        else 
            return null;
    }

    #endregion
}
