﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMo_Control : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Rocket")
        {
            StartCoroutine(MakeSlowMo_Coroutine());
        }
    }

    IEnumerator MakeSlowMo_Coroutine()
    {
        Time.timeScale /= 4;
        UIManager.Instance.PlaySlowMoEffect();
        yield return new WaitForSeconds(0.25f);
        if (Time.timeScale <= 0.25)
        {
            Time.timeScale = 1;
        }
    }
}
