﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Random = UnityEngine.Random;
using Debug = UnityEngine.Debug;

public class QuestionTriggerControl : MonoBehaviour
{
    public GameObject questionPoolGameObject;
    public GameObject questionTextGameObject;
    public List<GameObject> answerTextGameObjectList;

    [HideInInspector] public GameplayManager.Operation _chosenOperation;
    [HideInInspector] public int multiplier_1;  // 1st multiplier of the questions
    [HideInInspector] public int multiplier_2;  // 2nd multipler of the questions
    [HideInInspector] public int correctAnswer; // the answer of the random generated question
    [HideInInspector] public List<int> choices;
    [HideInInspector] public string rocQText;
    [HideInInspector] public bool isGlowing = false;
    private int value;

    private void Start()
    {
        choices = new List<int>(3);
    }

    private void OnTriggerEnter(Collider collision)
    {
        SFXManager.Instance.questionAS.Play();
        choices.Clear();

        for (int i = 0; i < 3; i++)
        {
            if(correctAnswer < 20)
            {
                value = Random.Range(0, 20);
            }
            else if(correctAnswer > 20 && correctAnswer < 100)
            {
                value = Random.Range(0, 100);
            }
            else if (correctAnswer > 100 && correctAnswer < 1000)
            {
                value = Random.Range(0, 1000);
            }
            CheckGeneratedChoice(choices);
        }

        int answerIndex = Random.Range(0, 3);
        
        if (!choices.Contains(correctAnswer))
        {
            choices[answerIndex] = correctAnswer;
        }

        for (int i = 0; i < 3; i++)
        {
            answerTextGameObjectList[i].GetComponent<TMPro.TMP_Text>().text = choices[i].ToString();
        }

        GameplayManager.Instance.inLevel_Counter++;
        questionPoolGameObject.GetComponent<QuestionPoolControl>().NextQuestion().GetComponent<QuestionTriggerControl>().CreateQuestion();
    }


    public void CreateQuestion()
    { 
        _chosenOperation = GameplayManager.Instance.chosenOperation;

        if(GameplayManager.Instance.numberDigit == 0 && _chosenOperation == GameplayManager.Operation.multiplication)
        {

            switch (GameplayManager.Instance.chosenLevel)
            {
                case 10:
                    multiplier_1 = Random.Range(1, 12);
                    multiplier_2 = Random.Range(2, 6);
                    break;
                case 11:
                    multiplier_1 = Random.Range(1, 15);
                    multiplier_2 = Random.Range(2, 6);
                    break;
                case 12:
                    multiplier_1 = Random.Range(1, 21);
                    multiplier_2 = Random.Range(1, 10);
                    break;
                default :
                    multiplier_1 = GameplayManager.Instance.chosenLevel;
                    multiplier_2 = GameplayManager.Instance.inLevel_Counter;
                    break;
            }
        }

        if (GameplayManager.Instance.numberDigit != 0 && (_chosenOperation == GameplayManager.Operation.addition || _chosenOperation == GameplayManager.Operation.subtraction))
        {
            switch (GameplayManager.Instance.numberDigit)
            {
                case 1:
                    multiplier_1 = Random.Range(1, 10);
                    multiplier_2 = Random.Range(1, 10);
                    break;
                case 2:
                    multiplier_1 = Random.Range(1, 100);
                    multiplier_2 = Random.Range(1, 100);
                    break;
                case 3:
                    multiplier_1 = Random.Range(1, 51) * 5;
                    multiplier_2 = Random.Range(1, 21) * 5;
                    break;
                case 100:
                    multiplier_1 = Random.Range(1, 17) * 50;
                    multiplier_2 = Random.Range(1, 10) * 10;
                    break;
            }
        }

        switch (_chosenOperation)
        {
            case GameplayManager.Operation.addition:
                questionTextGameObject.GetComponent<TMPro.TMP_Text>().text = multiplier_1 + " + " + multiplier_2 + " = ?";
                CheckFinishLine();
                correctAnswer = multiplier_1 + multiplier_2;
                break;

            case GameplayManager.Operation.subtraction:
                if (multiplier_2 > multiplier_1)
                {
                    questionTextGameObject.GetComponent<TMPro.TMP_Text>().text = multiplier_2 + " - " + multiplier_1 + " = ?";
                    CheckFinishLine();
                    correctAnswer = multiplier_2 - multiplier_1;
                }
                else
                {
                    questionTextGameObject.GetComponent<TMPro.TMP_Text>().text = multiplier_1 + " - " + multiplier_2 + " = ?";
                    CheckFinishLine();
                    correctAnswer = multiplier_1 - multiplier_2;
                }
                break;

            case GameplayManager.Operation.multiplication:
                questionTextGameObject.GetComponent<TMPro.TMP_Text>().text = multiplier_1 + " x " + multiplier_2 + " = ?";
                CheckFinishLine();
                correctAnswer = multiplier_1 * multiplier_2;
                break;
        }
    }

    public void CheckFinishLine()
    {
        if(GameplayManager.Instance.inLevel_Counter == 10)
        {
            questionTextGameObject.GetComponent<TMPro.TMP_Text>().text = "=== BİTİŞ ===";
        }
    }

    public bool CheckGeneratedChoice(List<int> list)
    {
        if (list.Contains(value))
        {
            
            value = Random.Range(0, 21);
            CheckGeneratedChoice(list);
            return false;
        }
        else
        {
            list.Add(value);
            return true;
        }
    }
}
