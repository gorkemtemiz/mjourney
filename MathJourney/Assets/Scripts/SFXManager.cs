﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : Singleton<SFXManager>
{
    public AudioSource mainThemeAS;
    public AudioSource gameplayTheme1AS;
    public AudioSource commonButtonClickAS;
    public AudioSource rocketButtonClickAS;
    public AudioSource planetButtonClickAS;
    public AudioSource numberButtonClickAS;
    public AudioSource questionAS;
    public AudioSource correctAnswerAS;
    public AudioSource wrongAnswerAS;
    public AudioSource crashAS;
    public AudioSource fuelBarrelAS;
    public AudioSource startLineAS;
    public AudioSource startAnimAS;
    public AudioSource countdownNumAS;
    public AudioSource countdownGoAS;
    public AudioSource levelEndAnimAS;
    public AudioSource fuelFullingAS;


    [HideInInspector] public bool isSoundsMuted;
     
    void Start()
    {
        mainThemeAS.Play();
    }

    private void OnEnable()
    {
        mainThemeAS.Play();
    }

    public void PlayMainThemeSFX()
    {
        mainThemeAS.Play();
    }

    public void PlayCommonButtonClickSFX()
    {
        commonButtonClickAS.Play();
    }

    public void PlayRocketButtonClickSFX()
    {
        rocketButtonClickAS.Play();
    }

    public void PlayPlanetButtonClickSFX()
    {
        planetButtonClickAS.Play();
    }

    public void PlayNumberButtonClickSFX()
    {
        numberButtonClickAS.Play();
    }

    public void PlayGameplayThemeRandomly()
    {
        StopAllSounds();
        gameplayTheme1AS.Play();         
    }

    public void PlayStartLineSFX()
    {
        startLineAS.Play();
    }
    
    public void PlayStartAnimSFX()
    {
        StartCoroutine(PlayStartAnimSFX_co());
    }

    private IEnumerator PlayStartAnimSFX_co()
    {
        startAnimAS.Play();
        yield return new WaitForSeconds(3f);
        countdownNumAS.Play();
        yield return new WaitForSeconds(1f);
        countdownNumAS.Play();
        yield return new WaitForSeconds(1f);
        countdownNumAS.Play();
        yield return new WaitForSeconds(1f);
        countdownGoAS.Play();
    }

    public void PlayLevelEndAnimSFX()
    {
        StartCoroutine(PlayLevelEndAnimSFX_co());
    }

    private IEnumerator PlayLevelEndAnimSFX_co()
    {
        StopAllSounds();
        levelEndAnimAS.Play();
        yield return new WaitForSeconds(3f);
        fuelFullingAS.Play();
        yield return new WaitForSeconds(2.5f);
        PlayMainThemeSFX();
    }

    public void PlayFuelBarrelSFX()
    {
        fuelBarrelAS.Play();
    }

    public void StopAllSounds()
    {
        mainThemeAS.Stop();
        gameplayTheme1AS.Stop();
    }
}
