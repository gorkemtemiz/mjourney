﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelBarrelCollisionControl : MonoBehaviour
{
    public ParticleSystem pickupEffect;
    public float rotationSpeed;
    public float yVar;

    private void FixedUpdate()
    {
        transform.Rotate(Vector3.up, Time.timeScale * rotationSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Rocket"))
        {
            StartCoroutine(UIManager.Instance.DoBarrelTriggerEffect(other.gameObject, gameObject, pickupEffect, 10));
        }
    }
}
