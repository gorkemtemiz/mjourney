﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketElements : MonoBehaviour
{
    public GameObject RocketFires;
    public GameObject RocketBoost;

    public void BoostTheRocket()
    {
        StartCoroutine(BoostTheRocket_Co());
    }

    IEnumerator BoostTheRocket_Co()
    {
        RocketFires.SetActive(false);
        RocketBoost.SetActive(true);
        GameplayManager.Instance.carCurrentSpeed += 50;
        yield return new WaitForSeconds(1.5f);
        GameplayManager.Instance.carCurrentSpeed -= 50;
        RocketFires.SetActive(true);
        RocketBoost.SetActive(false);
    }
}
