﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndestroyableObjectControl : MonoBehaviour
{
    public List<GameObject> undestroyables;

    void Start()
    {
        foreach (GameObject gameObject in undestroyables)
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
