﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObstacleCollisionControl : MonoBehaviour
{

    public ParticleSystem collisionEffectBoom;
    public ParticleSystem astroidCollisionPS;

    private void Awake()
    {
        collisionEffectBoom.GetComponent<ParticleSystem>();
        astroidCollisionPS.GetComponent<ParticleSystem>();

    }

    private void  OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Rocket"))
        {
            StartCoroutine(UIManager.Instance.DoObstacleTriggerEffect(other.gameObject, gameObject, collisionEffectBoom, astroidCollisionPS, -10));            
        }
    }
}
