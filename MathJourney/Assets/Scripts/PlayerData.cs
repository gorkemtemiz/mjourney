﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    public string playerName;
    public int playerAge;
    public int playerScore;
    public int answersCount;
    public int mistakesCount;
    public int rocketColorSet;
    //public List<int> playedOperations;  

    public void ResetPlayerObject()
    {
        playerName = "";
        playerAge = 0;
        playerScore = 0;
        answersCount = 0;
        mistakesCount = 0;
        //playedOperations.Clear();
    }

    public void ResetPlayerStats()
    {
        playerScore = 0;
        answersCount = 0;
        mistakesCount = 0;
    }
}
