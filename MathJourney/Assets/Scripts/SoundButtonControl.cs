﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SoundButtonControl : MonoBehaviour
{
    public Sprite soundOn_Texture;
    public Sprite soundOff_Texture;

    void Start()
    {
        if (UIManager.Instance.isSoundOff)
        {
            gameObject.GetComponent<Image>().sprite = soundOff_Texture;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = soundOn_Texture;
        }
    }

    private void OnEnable()
    {
        if (UIManager.Instance.isSoundOff)
        {
            gameObject.GetComponent<Image>().sprite = soundOff_Texture;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = soundOn_Texture;
        }
    }

    public void SwitchSound()
    {
        if (!UIManager.Instance.isSoundOff)
        {
            gameObject.GetComponent<Image>().sprite = soundOff_Texture;
            AudioListener.pause = true;
            UIManager.Instance.isSoundOff = true;
        }

        else
        {
            gameObject.GetComponent<Image>().sprite = soundOn_Texture;
            AudioListener.pause = false;
            UIManager.Instance.isSoundOff = false;
        }
    }
}
