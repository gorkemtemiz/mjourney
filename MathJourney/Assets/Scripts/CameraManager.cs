﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraManager : Singleton<CameraManager>
{
    [HideInInspector] public GameObject gameCamera;
    [HideInInspector] public GameObject rocket;
    private Vector3 gameCameraPos;
    private float offsetZ;
    
    
    //private void Start()
    //{
    //    gameCamera = GameObject.FindGameObjectWithTag("GameCamera");
    //    rocket = GameObject.FindGameObjectWithTag("Rocket");
    //    offsetZ = gameCamera.transform.position.z;
    //    gameCameraPos = gameCamera.transform.position;
    //    SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    //}

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }


    void Update()
    {
        if (rocket != null && rocket.activeInHierarchy)
        {
            gameCameraPos.x = rocket.transform.localPosition.x;
            gameCameraPos.z = rocket.transform.localPosition.z + offsetZ;
        }
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        gameCamera = GameObject.FindGameObjectWithTag("GameCamera");
        rocket = GameObject.FindGameObjectWithTag("Rocket");
        offsetZ = gameCamera.transform.position.z;
        gameCameraPos = gameCamera.transform.position;
    }
}


    
