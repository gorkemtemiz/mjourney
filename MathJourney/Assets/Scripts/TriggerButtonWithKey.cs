﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerButtonWithKey : MonoBehaviour
{
    public KeyCode key;

    private Button thisButton;

    // Start is called before the first frame update
    void Start()
    {
        thisButton = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            thisButton.onClick.Invoke();
        }
    }
}
