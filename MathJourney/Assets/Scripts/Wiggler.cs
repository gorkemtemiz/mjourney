﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wiggler : MonoBehaviour
{
	[SerializeField] float wiggleDistance = 0.001f;
	[SerializeField] float wiggleSpeed = 1f;
	public int turnCase;
	private float xPosition;
	private float yPosition;

    void FixedUpdate()
	{
        switch (turnCase)
        {
			case 1:
				xPosition = Mathf.Sin(Time.time * wiggleSpeed) * wiggleDistance *	1 / 100;
				yPosition = Mathf.Cos(Time.time * wiggleSpeed) * wiggleDistance * 1 / 100;
				transform.localPosition += new Vector3(xPosition, yPosition, 0);
				break;
			case 2:
				xPosition = Mathf.Sin(Time.time * wiggleSpeed) * wiggleDistance * -1 / 100;
				yPosition = Mathf.Cos(Time.time * wiggleSpeed) * wiggleDistance * 1 / 100;
				transform.localPosition += new Vector3(xPosition, yPosition, 0);
				break;
			case 3:
				xPosition = Mathf.Sin(Time.time * wiggleSpeed) * wiggleDistance * -1 / 100;
				yPosition = Mathf.Cos(Time.time * wiggleSpeed) * wiggleDistance * -1 / 100;
				transform.localPosition += new Vector3(xPosition, yPosition, 0);
				break;
			case 4:
				xPosition = Mathf.Sin(Time.time * wiggleSpeed) * wiggleDistance * 1 / 100;
				yPosition = Mathf.Cos(Time.time * wiggleSpeed) * wiggleDistance * -1 / 100;
				transform.localPosition += new Vector3(xPosition, yPosition, 0);
				break;
		}
	}


	IEnumerator DoWiggle(float a)
	{
	    if(gameObject.activeInHierarchy)
	    {
			transform.DOLocalMove(new Vector3(transform.localPosition.x + a, transform.localPosition.y + a), 0.1f);
	
	        yield return null;
	    }
	}
}
